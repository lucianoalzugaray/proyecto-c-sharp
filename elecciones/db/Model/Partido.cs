﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class Partido 
    {
        //"id","nro_partido", "nom_partido", "color"
        #region variables locales
        private int _id;
		private int _nro_partido;
        private string _nom_partido; 
        private string _color;
        
        #endregion
        
        #region propiedades publicas
				
		public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int Nro_Partido
        {
            get { return _nro_partido; }
            set { _nro_partido = value; }
        }

        public string Nom_Partido
        {
            get { return _nom_partido; }
            set { _nom_partido = value; }
        }

        public string Color
        {
            get { return _color; }
            set { _color = value; }
        }
        #endregion
    }
}

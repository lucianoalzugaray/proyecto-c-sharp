﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using elecciones.db;

namespace elecciones
{

    public enum OperacionForm
    {
        frmConsulta = 0, frmAlta = 1, frmModificacion = 2
    }

    public partial class CircuitoAMForm : Form
    {
        OperacionForm operacion = OperacionForm.frmConsulta; /**/
        Circuito c;
        //"id","letra", "secc_id", "nombre", "localidad_id"
        public CircuitoAMForm()
        {
            InitializeComponent();
        }

        public void ShowCircuito(Circuito circuito)
        {
            this.operacion = OperacionForm.frmModificacion;
            this.Titulo.Text = "Modificacion de Circuito";
            c = circuito;
            this.Id_TextBox.Text = c.Id.ToString();
            this.Letra_TextBox.Text = c.Letra;
            this.Seccion_TextBox.Text = c.Secc_Id.ToString();
            this.Nombre_TextBox.Text = c.Nombre;
            this.Localidad_TextBox.Text = c.Localidad_Id.ToString();
            this.ShowDialog();
        }

        public void NewCircuito()
        {
            this.Titulo.Text = "Nuevo Circuito";
            this.operacion = OperacionForm.frmAlta;
            this.ShowDialog();
        }

        private void CancelarBtn_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void AceptarBtn_Click(object sender, EventArgs e)
        {
            //"id","letra", "secc_id", "nombre", "localidad_id"
            if (this.operacion == OperacionForm.frmAlta)
            {
                c = new Circuito();
                c.Id = Convert.ToInt32(this.Id_TextBox.Text);
            }
            c.Letra = this.Letra_TextBox.Text;
            c.Secc_Id = Convert.ToInt32(this.Seccion_TextBox.Text);
            c.Nombre = this.Nombre_TextBox.Text;
            c.Localidad_Id = Convert.ToInt32(this.Localidad_TextBox.Text);
            c.saveObj();
            this.Dispose();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class Escuela : CommonObj, IAccessDB<Escuela>, ITable
    {

        private string[] _columns = { "id","circ_id", "nom_escuela", "direccion", "localidad_id" };
        
        public List<Escuela> findAll()
        {
            return this.findAll(null);
        }
        public List<Escuela> findAll(string criterio)
        {
            return ManagerDB<Escuela>.findAll(criterio);
        }
        public Escuela findbykey(params object[] key)
        {
            //"id","circ_id", "nom_escuela", "direccion", "localidad_id"  
            Escuela esc = (Escuela)ManagerDB<Escuela>.findbyKey(key);
            this.Id = esc.Id;
            this.Circ_Id = esc.Circ_Id;
            this.Nom_Escuela = esc.Nom_Escuela;
            this.Direccion = esc.Direccion;
            this.Localidad_Id = esc.Localidad_Id;
            this.IsNew = false;
            return this;
        }

        public bool saveObj()
        {
            return ManagerDB<Escuela>.saveObject(this);
        }

        public string TableName
        {
            get { return "escuela"; }
        }

        public string KeyTable
        {
            get { return "id"; }
        }

        public void initialize(System.Data.DataRow dr)
        {
            //"id","circ_id", "nom_escuela", "direccion", "localidad_id"  
            this._id = Int32.Parse(dr[_columns[0]].ToString());
			this._circ_id = Int32.Parse(dr[_columns[1]].ToString());
            this._nom_escuela = dr[_columns[2]].ToString();
            this._direccion = dr[_columns[3]].ToString();
            this._localidad_id = Int32.Parse(dr[_columns[4]].ToString());
         
            this.IsNew = false;
        }

        public string[] columns
        {
            get { return _columns; }
        }

        private string[] list_values()
        {
            //"id","circ_id", "nom_escuela", "direccion", "localidad_id"  
            string[] values = { (this.IsNew?"":_columns[0] + "=")+this._id.ToString(),
								(this.IsNew?"":_columns[1] + "=")+this._circ_id.ToString(),
                                (this.IsNew?"":_columns[2] + "=")+this._nom_escuela,
								(this.IsNew?"":_columns[3] + "=")+this._direccion,
								(this.IsNew?"":_columns[4] + "=")+this._localidad_id.ToString()
                              };
            return values;
        }

        public string SqlString
        {
            get
            {
                string vvalues = String.Join(",", this.list_values());
                string sqliu = (this.IsNew ? "insert into {0} ({1}) values ({2})" : "update  {0} set {1} where {2}");
                return String.Format(sqliu, this.TableName, (this.IsNew ? String.Join(",", _columns) : vvalues), (this.IsNew ? vvalues : String.Format("id = {0}", this.Id)));
            }
        }

        public void setKeyValue(object valueId)
        {
            throw new NotImplementedException();
        }

        public string sqlKeyWhere(params object[] values)
        {
            return String.Format("id = {0}", values[0]);
        }
    }
}

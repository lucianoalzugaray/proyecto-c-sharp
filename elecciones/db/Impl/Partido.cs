﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class Partido : CommonObj, IAccessDB<Partido>, ITable
    {
        //"id","nro_partido", "nom_partido", "color"
        private string[] _columns = { "id", "nro_partido", "nom_partido", "color" };

        public List<Partido> findAll()
        {
            return this.findAll(null);
        }
        public List<Partido> findAll(string criterio)
        {
            return ManagerDB<Partido>.findAll(criterio);
        }
        public Partido findbykey(params object[] key)
        {
            //"id","nro_partido", "nom_partido", "color"
            Partido par = (Partido)ManagerDB<Partido>.findbyKey(key);
            this.Id = par.Id;
            this.Nro_Partido = par.Nro_Partido;
            this.Nom_Partido = par.Nom_Partido;
            this.Color = par.Color;
            
            this.IsNew = false;
            return this;
        }

        public bool saveObj()
        {
            return ManagerDB<Partido>.saveObject(this);
        }

        public string TableName
        {
            get { return "partido"; }
        }

        public string KeyTable
        {
            get { return "id"; }
        }

        public void initialize(System.Data.DataRow dr)
        {
            //"id","nro_partido", "nom_partido", "color"
            this._id = Int32.Parse(dr[_columns[0]].ToString());
            this._nro_partido = Int32.Parse(dr[_columns[1]].ToString());
            this._nom_partido = dr[_columns[2]].ToString();
            this._color = dr[_columns[3]].ToString();
            
            this.IsNew = false;
        }

        public string[] columns
        {
            get { return _columns; }
        }

        private string[] list_values()
        {
            //"id","nro_partido", "nom_partido", "color"
            string[] values = { (this.IsNew?"":_columns[0] + "=")+this._id.ToString(),
								(this.IsNew?"":_columns[1] + "=")+this._nro_partido.ToString(),
								(this.IsNew?"":_columns[2] + "=")+this._nom_partido,
								(this.IsNew?"":_columns[3] + "=")+this._color
                              };
            return values;
        }

        public string SqlString
        {
            get
            {
                string vvalues = String.Join(",", this.list_values());
                string sqliu = (this.IsNew ? "insert into {0} ({1}) values ({2})" : "update  {0} set {1} where {2}");
                return String.Format(sqliu, this.TableName, (this.IsNew ? String.Join(",", _columns) : vvalues), (this.IsNew ? vvalues : String.Format("id = {0}", this.Id)));
            }
        }

        public void setKeyValue(object valueId)
        {
            throw new NotImplementedException();
        }

        public string sqlKeyWhere(params object[] values)
        {
            return String.Format("id = {0}", values[0]);
        }
    }
}

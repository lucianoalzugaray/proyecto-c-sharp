﻿namespace elecciones
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EscuelasButton = new System.Windows.Forms.Button();
            this.PartidosButton = new System.Windows.Forms.Button();
            this.SeccionesButton = new System.Windows.Forms.Button();
            this.MesaButton = new System.Windows.Forms.Button();
            this.dataGrid = new System.Windows.Forms.DataGridView();
            this.CircuitosButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // EscuelasButton
            // 
            this.EscuelasButton.Location = new System.Drawing.Point(12, 12);
            this.EscuelasButton.Name = "EscuelasButton";
            this.EscuelasButton.Size = new System.Drawing.Size(75, 23);
            this.EscuelasButton.TabIndex = 0;
            this.EscuelasButton.Text = "Escuelas";
            this.EscuelasButton.UseVisualStyleBackColor = true;
            this.EscuelasButton.Click += new System.EventHandler(this.EscuelasButton_Click);
            // 
            // PartidosButton
            // 
            this.PartidosButton.Location = new System.Drawing.Point(13, 42);
            this.PartidosButton.Name = "PartidosButton";
            this.PartidosButton.Size = new System.Drawing.Size(75, 23);
            this.PartidosButton.TabIndex = 1;
            this.PartidosButton.Text = "Partidos";
            this.PartidosButton.UseVisualStyleBackColor = true;
            this.PartidosButton.Click += new System.EventHandler(this.PartidosButton_Click);
            // 
            // SeccionesButton
            // 
            this.SeccionesButton.Location = new System.Drawing.Point(13, 72);
            this.SeccionesButton.Name = "SeccionesButton";
            this.SeccionesButton.Size = new System.Drawing.Size(75, 23);
            this.SeccionesButton.TabIndex = 2;
            this.SeccionesButton.Text = "Secciones";
            this.SeccionesButton.UseVisualStyleBackColor = true;
            this.SeccionesButton.Click += new System.EventHandler(this.SeccionesButton_Click);
            // 
            // MesaButton
            // 
            this.MesaButton.Location = new System.Drawing.Point(13, 102);
            this.MesaButton.Name = "MesaButton";
            this.MesaButton.Size = new System.Drawing.Size(75, 23);
            this.MesaButton.TabIndex = 3;
            this.MesaButton.Text = "Mesa";
            this.MesaButton.UseVisualStyleBackColor = true;
            this.MesaButton.Click += new System.EventHandler(this.MesaButton_Click);
            // 
            // dataGrid
            // 
            this.dataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGrid.Location = new System.Drawing.Point(120, 12);
            this.dataGrid.Name = "dataGrid";
            this.dataGrid.Size = new System.Drawing.Size(591, 285);
            this.dataGrid.TabIndex = 4;
            // 
            // CircuitosButton
            // 
            this.CircuitosButton.Location = new System.Drawing.Point(13, 198);
            this.CircuitosButton.Name = "CircuitosButton";
            this.CircuitosButton.Size = new System.Drawing.Size(75, 23);
            this.CircuitosButton.TabIndex = 5;
            this.CircuitosButton.Text = "Ver Circuitos";
            this.CircuitosButton.UseVisualStyleBackColor = true;
            this.CircuitosButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(723, 343);
            this.Controls.Add(this.CircuitosButton);
            this.Controls.Add(this.dataGrid);
            this.Controls.Add(this.MesaButton);
            this.Controls.Add(this.SeccionesButton);
            this.Controls.Add(this.PartidosButton);
            this.Controls.Add(this.EscuelasButton);
            this.Name = "MainView";
            this.Text = "MainView";
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button EscuelasButton;
        private System.Windows.Forms.Button PartidosButton;
        private System.Windows.Forms.Button SeccionesButton;
        private System.Windows.Forms.Button MesaButton;
        private System.Windows.Forms.DataGridView dataGrid;
        private System.Windows.Forms.Button CircuitosButton;
    }
}
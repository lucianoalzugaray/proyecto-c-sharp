﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class TelegramaActaVotos : CommonObj, IAccessDB<TelegramaActaVotos>, ITable
    {
        //"id","teleg_id", "lista_partido_loc_id", "votos"
        private string[] _columns = { "id","teleg_id", "lista_partido_loc_id", "votos" };

        public List<TelegramaActaVotos> findAll()
        {
            return this.findAll(null);
        }
        public List<TelegramaActaVotos> findAll(string criterio)
        {
            return ManagerDB<TelegramaActaVotos>.findAll(criterio);
        }
        public TelegramaActaVotos findbykey(params object[] key)
        {
            //"id","teleg_id", "lista_partido_loc_id", "votos"
            TelegramaActaVotos tav = (TelegramaActaVotos)ManagerDB<TelegramaActaVotos>.findbyKey(key);
            this.Id = tav.Id;
            this.Lista_Partido_Loc_Id = tav.Lista_Partido_Loc_Id;
            this.Teleg_Id = tav.Teleg_Id;
            this.Votos = tav.Votos;
            this.IsNew = false;
            return this;
        }

        public bool saveObj()
        {
            return ManagerDB<TelegramaActaVotos>.saveObject(this);
        }

        public string TableName
        {
            get { return "telegrama_acta_votos"; }
        }

        public string KeyTable
        {
            get { return "id"; }
        }

        public void initialize(System.Data.DataRow dr)
        {
            //"id","teleg_id", "lista_partido_loc_id", "votos"
            this._id = Int32.Parse(dr[_columns[0]].ToString());
            this._teleg_id = Int32.Parse(dr[_columns[1]].ToString());
            this._lista_partido_loc_id = Int32.Parse(dr[_columns[2]].ToString());
            this._votos = Int32.Parse(dr[_columns[3]].ToString());
            this.IsNew = false;
        }

        public string[] columns
        {
            get { return _columns; }
        }

        private string[] list_values()
        {
            //"id","teleg_id", "lista_partido_loc_id", "votos"
            string[] values = { (this.IsNew?"":_columns[0] + "=")+this._id.ToString(),
								(this.IsNew?"":_columns[1] + "=")+this._teleg_id.ToString(),
                                (this.IsNew?"":_columns[2] + "=")+this._lista_partido_loc_id.ToString(),
                                (this.IsNew?"":_columns[3] + "=")+this._votos.ToString()

							  };
            return values;
        }

        public string SqlString
        {
            get
            {
                string vvalues = String.Join(",", this.list_values());
                string sqliu = (this.IsNew ? "insert into {0} ({1}) values ({2})" : "update  {0} set {1} where {2}");
                return String.Format(sqliu, this.TableName, (this.IsNew ? String.Join(",", _columns) : vvalues), (this.IsNew ? vvalues : String.Format("id = {0}", this.Id)));
            }
        }

        public void setKeyValue(object valueId)
        {
            throw new NotImplementedException();
        }

        public string sqlKeyWhere(params object[] values)
        {
            return String.Format("id = {0}", values[0]);
        }

    }
}

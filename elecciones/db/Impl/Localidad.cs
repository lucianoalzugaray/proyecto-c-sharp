﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class Localidad : CommonObj, IAccessDB<Localidad>, ITable
    {
        //"id","nom_localidad"
        private string[] _columns = { "id", "nom_localidad" };

        public List<Localidad> findAll()
        {
            return this.findAll(null);
        }
        public List<Localidad> findAll(string criterio)
        {
            return ManagerDB<Localidad>.findAll(criterio);
        }
        public Localidad findbykey(params object[] key)
        {
            //"id","nom_localidad"
            Localidad loc = (Localidad)ManagerDB<Localidad>.findbyKey(key);
            this.Id = loc.Id;
            this.Nom_Localidad = loc.Nom_Localidad;
            this.IsNew = false;
            return this;
        }

        public bool saveObj()
        {
            return ManagerDB<Localidad>.saveObject(this);
        }

        public string TableName
        {
            get { return "localidad"; }
        }

        public string KeyTable
        {
            get { return "id"; }
        }

        public void initialize(System.Data.DataRow dr)
        {
            //"id","nom_localidad"
            this._id = Int32.Parse(dr[_columns[0]].ToString());
			this._nom_localidad = dr[_columns[1]].ToString();
            
            this.IsNew = false;
        }

        public string[] columns
        {
            get { return _columns; }
        }

        private string[] list_values()
        {
            //"id","nom_localidad"
            string[] values = { (this.IsNew?"":_columns[0] + "=")+this._id.ToString(),
								(this.IsNew?"":_columns[1] + "=")+this._nom_localidad,
                              };
            return values;
        }

        public string SqlString
        {
            get
            {
                string vvalues = String.Join(",", this.list_values());
                string sqliu = (this.IsNew ? "insert into {0} ({1}) values ({2})" : "update  {0} set {1} where {2}");
                return String.Format(sqliu, this.TableName, (this.IsNew ? String.Join(",", _columns) : vvalues), (this.IsNew ? vvalues : String.Format("id = {0}", this.Id)));
            }
        }

        public void setKeyValue(object valueId)
        {
            throw new NotImplementedException();
        }

        public string sqlKeyWhere(params object[] values)
        {
            return String.Format("id = {0}", values[0]);
        }
    }
}

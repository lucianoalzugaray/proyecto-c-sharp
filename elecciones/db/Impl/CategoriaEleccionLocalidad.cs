﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class CategoriaEleccionLocalidad : CommonObj, IAccessDB<CategoriaEleccionLocalidad>, ITable
    {
        private string[] _columns = { "id","localidad_id", "elige_gob","elige_dip","elige_cm","elige_int", "elige_cd","elige_jc"};
        public List<CategoriaEleccionLocalidad> findAll()
        {
            return this.findAll(null);
        }
        public List<CategoriaEleccionLocalidad> findAll(string criterio)
        {
            return ManagerDB<CategoriaEleccionLocalidad>.findAll(criterio);
        }
        public CategoriaEleccionLocalidad findbykey(params object[] key)
        {
            CategoriaEleccionLocalidad cel = (CategoriaEleccionLocalidad)ManagerDB<CategoriaEleccionLocalidad>.findbyKey(key);
            this.Id = cel.Id;
            this.Localidad_Id = cel.Localidad_Id;
            this.Elige_Gob = cel.Elige_Gob;
            this.Elige_Dip = cel.Elige_Dip;
            this.Elige_Cm = cel.Elige_Cm;
            this.Elige_Int = cel.Elige_Int;
            this.Elige_Cd = cel.Elige_Cd;
            this.Elige_Jc = cel.Elige_Jc;
 
            this.IsNew = false;
            return this;
        }
        public bool saveObj()
        {
            return ManagerDB<CategoriaEleccionLocalidad>.saveObject(this);
        }

        public string TableName
        {
            get { return "categoria_eleccion_localidad"; }
        }

        public string KeyTable
        {
            get { return "id"; }
        }

        public void initialize(System.Data.DataRow dr)
        {
            // "id","localidad_id", "elige_gob","elige_dip","elige_cm","elige_int", "elige_cd","elige_jc"
            this._id = Int32.Parse(dr[_columns[0]].ToString());
			this._localidad_id = Int32.Parse(dr[_columns[1]].ToString());
            this._elige_gob = Int32.Parse(dr[_columns[2]].ToString()) == 1;
            this._elige_dip = Int32.Parse(dr[_columns[3]].ToString()) == 1;
            this._elige_cm = Int32.Parse(dr[_columns[4]].ToString()) == 1;
            this._elige_int = Int32.Parse(dr[_columns[5]].ToString()) == 1;
            this._elige_cd = Int32.Parse(dr[_columns[6]].ToString()) == 1;
            this._elige_jc = Int32.Parse(dr[_columns[7]].ToString()) == 1;
            this.IsNew = false;
        }

        public string[] columns
        {
            get { return _columns; }
        }

        private string[] list_values()
        {
            // "id","localidad_id", "elige_gob","elige_dip","elige_cm","elige_int", "elige_cd","elige_jc"
            string[] values = { (this.IsNew?"":_columns[0] + "=")+this._id.ToString(),
								(this.IsNew?"":_columns[1] + "=")+this._localidad_id.ToString(),
                                (this.IsNew?"":_columns[2] + "=")+(this._elige_gob ? '1':'0'),
                                (this.IsNew?"":_columns[3] + "=")+(this._elige_dip? '1':'0'),
                                (this.IsNew?"":_columns[4] + "=")+(this._elige_cm? '1':'0'),
                                (this.IsNew?"":_columns[5] + "=")+(this._elige_int? '1':'0'),
                                (this.IsNew?"":_columns[6] + "=")+(this._elige_cd? '1':'0'),
                                (this.IsNew?"":_columns[7] + "=")+(this._elige_jc? '1':'0')
                              };
            return values;
        }

        public string SqlString
        {
            get
            {
                string vvalues = String.Join(",", this.list_values());
                string sqliu = (this.IsNew ? "insert into {0} ({1}) values ({2})" : "update  {0} set {1} where {2}");
                return String.Format(sqliu, this.TableName, (this.IsNew ? String.Join(",", _columns) : vvalues), (this.IsNew ? vvalues : String.Format("id = {0}", this.Id)));
            }
        }

        public void setKeyValue(object valueId)
        {
            throw new NotImplementedException();
        }

        public string sqlKeyWhere(params object[] values)
        {
            return String.Format("id = {0}", values[0]);
        }
    }
}

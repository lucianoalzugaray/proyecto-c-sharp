﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using elecciones.db;

namespace elecciones
{
    public partial class CircuitoFrm : Form
    {
        public CircuitoFrm()
        {
            InitializeComponent();
        }

        private void CircuitoFrm_Load(object sender, EventArgs e)
        {
            /*
             * Se requiere este seteo para que se posibilite el mapeo de columnas que se Agregaron
             * desde el diseñador, Click con boton derecho sobre seleccion de grilla -> Edit Columns
            */
            this.gridData.AutoGenerateColumns = false;
            List<Circuito>lista = ManagerDB<Circuito>.findAll();
            this.gridData.DataSource = lista;
            
        }


        private void CerrarButton_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void NuevoButton_Click(object sender, EventArgs e)
        {
            CircuitoAMForm frm = new CircuitoAMForm();

            frm.NewCircuito();
        }

        private void CerrarBtn_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void gridData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                CircuitoAMForm frm = new CircuitoAMForm();
                frm.ShowCircuito(senderGrid.Rows[e.RowIndex].DataBoundItem as Circuito);
            }
        }

    }
}

        

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class Escuela 
    {
        //"id","circ_id", "nom_escuela", "direccion", "localidad_id"  
        #region variables locales
        private int _id;
		private int _circ_id;
        private string _nom_escuela;
        private string _direccion;
        private int _localidad_id;
        #endregion
        
        #region propiedades publicas
				
		public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int Circ_Id
        {
            get { return _circ_id; }
            set { _circ_id = value; }
        }

		public string Nom_Escuela
        {
            get { return _nom_escuela; }
            set { _nom_escuela = value; }
        }

        public string Direccion
        {
            get { return _direccion; }
            set { _direccion = value; }
        }

        public int Localidad_Id
        {
            get { return _localidad_id; }
            set { _localidad_id = value; }
        }
        #endregion
    }
}

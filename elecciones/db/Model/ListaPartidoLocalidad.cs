﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class ListaPartidoLocalidad
    {
        //"id","lista_partido_id", "categoria_id", "localidad_id"
        #region variables locales
        private int _id;
		private int _lista_partido_id;
        private int _categoria_id;
        private int _localidad_id;
        #endregion
        
        #region propiedades publicas
				
		public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int Lista_Partido_Id
        {
            get { return _lista_partido_id; }
            set { _lista_partido_id = value; }
        }

        public int Categoria_Id
        {
            get { return _categoria_id; }
            set { _categoria_id = value; }
        }

        public int Localidad_Id
        {
            get { return _localidad_id; }
            set { _localidad_id = value; }
        }
        #endregion
    }
}

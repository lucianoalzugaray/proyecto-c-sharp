﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class Localidad
    {
        //"id","nom_localidad"
        #region variables locales
        private int _id;
		private string _nom_localidad;
        #endregion
        
        #region propiedades publicas
				
		public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Nom_Localidad
        {
            get { return _nom_localidad; }
            set { _nom_localidad = value; }
        }
        #endregion
    }
}

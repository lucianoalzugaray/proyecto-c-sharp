﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Npgsql; // Libreria para conectar con PostgreSQL
using elecciones.db; // incluir libreria para poder acceder a los objetos de negocios.

namespace elecciones
{
    public partial class MainView : Form
    {
        public MainView()
        {
            InitializeComponent();
        }

        private void MainView_Load(object sender, EventArgs e)
        {
            this.dataGrid.DataSource = ManagerDB<Circuito>.findAll();
        }

        private void EscuelasButton_Click(object sender, EventArgs e)
        {
            this.dataGrid.DataSource = ManagerDB<Escuela>.findAll(); 
        }

        private void PartidosButton_Click(object sender, EventArgs e)
        {
            this.dataGrid.DataSource = ManagerDB<Partido>.findAll(); 
        }

        private void SeccionesButton_Click(object sender, EventArgs e)
        {
            this.dataGrid.DataSource = ManagerDB<Seccion>.findAll(); 
        }

        private void MesaButton_Click(object sender, EventArgs e)
        {
            this.dataGrid.DataSource = ManagerDB<Mesa>.findAll(); 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CircuitoFrm circ_form = new CircuitoFrm();
            circ_form.Show();
        }
    
    }
}

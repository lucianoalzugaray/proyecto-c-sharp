﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class CategoriaEleccionLocalidad
    {

        #region variables locales
        private int _id;
        private int _localidad_id;
        private bool _elige_gob;
        private bool _elige_dip;
        private bool _elige_cm;
        private bool _elige_int;
        private bool _elige_cd;
        private bool _elige_jc;
        #endregion

        #region propiedades publicas

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int Localidad_Id
        {
            get { return _localidad_id; }
            set { _localidad_id = value; }
        }


        public bool Elige_Gob
        {
            get { return _elige_gob; }
            set { _elige_gob = value; }
        }

        public bool Elige_Dip
        {
            get { return _elige_dip; }
            set { _elige_dip = value; }
        }

        public bool Elige_Cm
        {
            get { return _elige_cm; }
            set { _elige_cm = value; }
        }

        public bool Elige_Int
        {
            get { return _elige_int; }
            set { _elige_int = value; }
        }

        public bool Elige_Cd
        {
            get { return _elige_cd; }
            set { _elige_cd = value; }
        }

        public bool Elige_Jc
        {
            get { return _elige_jc; }
            set { _elige_jc = value; }
        }

        #endregion

    }
}

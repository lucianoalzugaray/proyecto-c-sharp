﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class Mesa : CommonObj, IAccessDB<Mesa>, ITable
    {
        //"id","nro_mesa", "circ_id", "esc_id", "cant_electores"
        private string[] _columns = { "id", "nro_mesa", "circ_id", "esc_id", "cant_electores" };

        public List<Mesa> findAll()
        {
            return this.findAll(null);
        }
        public List<Mesa> findAll(string criterio)
        {
            return ManagerDB<Mesa>.findAll(criterio);
        }
        public Mesa findbykey(params object[] key)
        {
            //"id","nro_mesa", "circ_id", "esc_id", "cant_electores"
            Mesa mesa = (Mesa)ManagerDB<Mesa>.findbyKey(key);
            this.Id = mesa.Id;
            this.Nro_Mesa = mesa.Nro_Mesa;
            this.Circ_Id = mesa.Circ_Id;
            this.Esc_Id = mesa.Esc_Id;
            this.Cant_Electores = mesa.Cant_Electores;
            this.IsNew = false;
            return this;
        }

        public bool saveObj()
        {
            return ManagerDB<Mesa>.saveObject(this);
        }

        public string TableName
        {
            get { return "mesa"; }
        }

        public string KeyTable
        {
            get { return "id"; }
        }

        public void initialize(System.Data.DataRow dr)
        {
            //"id","nro_mesa", "circ_id", "esc_id", "cant_electores"
            this._id = Int32.Parse(dr[_columns[0]].ToString());
            this._nro_mesa = Int32.Parse(dr[_columns[1]].ToString());
            this._circ_id = Int32.Parse(dr[_columns[2]].ToString());
            this._esc_id = Int32.Parse(dr[_columns[3]].ToString());
            this._cant_electores = Int32.Parse(dr[_columns[4]].ToString());
            
            this.IsNew = false;
        }

        public string[] columns
        {
            get { return _columns; }
        }

        private string[] list_values()
        {
            //"id","nom_localidad"
            string[] values = { (this.IsNew?"":_columns[0] + "=")+this._id.ToString(),
								(this.IsNew?"":_columns[1] + "=")+this._nro_mesa.ToString(),
								(this.IsNew?"":_columns[2] + "=")+this._circ_id.ToString(),
								(this.IsNew?"":_columns[3] + "=")+this._esc_id.ToString(),
								(this.IsNew?"":_columns[4] + "=")+this._cant_electores.ToString()
                              };
            return values;
        }

        public string SqlString
        {
            get
            {
                string vvalues = String.Join(",", this.list_values());
                string sqliu = (this.IsNew ? "insert into {0} ({1}) values ({2})" : "update  {0} set {1} where {2}");
                return String.Format(sqliu, this.TableName, (this.IsNew ? String.Join(",", _columns) : vvalues), (this.IsNew ? vvalues : String.Format("id = {0}", this.Id)));
            }
        }

        public void setKeyValue(object valueId)
        {
            throw new NotImplementedException();
        }

        public string sqlKeyWhere(params object[] values)
        {
            return String.Format("id = {0}", values[0]);
        }
    }
}

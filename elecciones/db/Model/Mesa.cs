﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class Mesa
    {
        //"id","nro_mesa", "circ_id", "esc_id", "cant_electores"
        #region variables locales
        private int _id;
		private int _nro_mesa;
        private int _circ_id; 
        private int _esc_id;
        private int _cant_electores;

        #endregion
        
        #region propiedades publicas
				
		public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int Nro_Mesa
        {
            get { return _nro_mesa; }
            set { _nro_mesa = value; }
        }

        public int Circ_Id
        {
            get { return _circ_id; }
            set { _circ_id = value; }
        }

        public int Cant_Electores
        {
            get { return _cant_electores; }
            set { _cant_electores = value; }
        }

        public int Esc_Id
        {
            get { return _esc_id; }
            set { _esc_id = value; }
        }
        #endregion
    }
}

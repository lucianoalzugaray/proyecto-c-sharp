﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class CategoriaEleccion
    {

        #region variables locales
        private int _id;
		private string _nom_categoria;
        #endregion
        
        #region propiedades publicas
				
		public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        
		public string Nom_Categoria
        {
            get { return _nom_categoria; }
            set { _nom_categoria = value; }
        }
        #endregion
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class ListaPartido : CommonObj, IAccessDB<ListaPartido>, ITable
    {
        private string[] _columns = { "id","partido_id", "nom_lista"};

        public List<ListaPartido> findAll()
        {
            return this.findAll(null);
        }
        public List<ListaPartido> findAll(string criterio)
        {
            return ManagerDB<ListaPartido>.findAll(criterio);
        }
        public ListaPartido findbykey(params object[] key)
        {
            // "id","partido_id", "nom_lista"
            ListaPartido lp = (ListaPartido)ManagerDB<ListaPartido>.findbyKey(key);
            this.Id = lp.Id;
            this.Partido_Id = lp.Partido_Id;
            this.Nom_Lista = lp.Nom_Lista;
            this.IsNew = false;
            return this;
        }

        public bool saveObj()
        {
            return ManagerDB<ListaPartido>.saveObject(this);
        }

        public string TableName
        {
            get { return "lista_partido"; }
        }

        public string KeyTable
        {
            get { return "id"; }
        }

        public void initialize(System.Data.DataRow dr)
        {
            // "id","partido_id", "nom_lista"
            this._id = Int32.Parse(dr[_columns[0]].ToString());
			this._partido_id = Int32.Parse(dr[_columns[1]].ToString());
            this._nom_lista = dr[_columns[2]].ToString();
            
            this.IsNew = false;
        }

        public string[] columns
        {
            get { return _columns; }
        }

        private string[] list_values()
        {
            // "id","partido_id", "nom_lista"
            string[] values = { (this.IsNew?"":_columns[0] + "=")+this._id.ToString(),
								(this.IsNew?"":_columns[1] + "=")+this._partido_id.ToString(),
								(this.IsNew?"":_columns[2] + "=")+this._nom_lista
                              };
            return values;
        }

        public string SqlString
        {
            get
            {
                string vvalues = String.Join(",", this.list_values());
                string sqliu = (this.IsNew ? "insert into {0} ({1}) values ({2})" : "update  {0} set {1} where {2}");
                return String.Format(sqliu, this.TableName, (this.IsNew ? String.Join(",", _columns) : vvalues), (this.IsNew ? vvalues : String.Format("id = {0}", this.Id)));
            }
        }

        public void setKeyValue(object valueId)
        {
            throw new NotImplementedException();
        }

        public string sqlKeyWhere(params object[] values)
        {
            return String.Format("id = {0}", values[0]);
        }
    }
}

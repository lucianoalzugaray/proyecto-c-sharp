﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class ListaPartidoLocalidad : CommonObj, IAccessDB<ListaPartidoLocalidad>, ITable
    {
        //"id","lista_partido_id", "categoria_id", "localidad_id"
        private string[] _columns = { "id","lista_partido_id", "categoria_id", "localidad_id"};

        public List<ListaPartidoLocalidad> findAll()
        {
            return this.findAll(null);
        }
        public List<ListaPartidoLocalidad> findAll(string criterio)
        {
            return ManagerDB<ListaPartidoLocalidad>.findAll(criterio);
        }
        public ListaPartidoLocalidad findbykey(params object[] key)
        {
            //"id","lista_partido_id", "categoria_id", "localidad_id"
            ListaPartidoLocalidad lp = (ListaPartidoLocalidad)ManagerDB<ListaPartidoLocalidad>.findbyKey(key);
            this.Id = lp.Id;
            this.Lista_Partido_Id = lp.Lista_Partido_Id;
            this.Categoria_Id = lp.Categoria_Id;
            this.Localidad_Id = lp.Localidad_Id;
            this.IsNew = false;
            return this;
        }

        public bool saveObj()
        {
            return ManagerDB<ListaPartidoLocalidad>.saveObject(this);
        }

        public string TableName
        {
            get { return "lista_partido_localidad"; }
        }

        public string KeyTable
        {
            get { return "id"; }
        }

        public void initialize(System.Data.DataRow dr)
        {
            //"id","lista_partido_id", "categoria_id", "localidad_id"
            this._id = Int32.Parse(dr[_columns[0]].ToString());
			this._lista_partido_id = Int32.Parse(dr[_columns[1]].ToString());
            this._categoria_id = Int32.Parse(dr[_columns[2]].ToString());
            this._localidad_id = Int32.Parse(dr[_columns[3]].ToString());
            
            this.IsNew = false;
        }

        public string[] columns
        {
            get { return _columns; }
        }

        private string[] list_values()
        {
            //"id","lista_partido_id", "categoria_id", "localidad_id"
            string[] values = { (this.IsNew?"":_columns[0] + "=")+this._id.ToString(),
								(this.IsNew?"":_columns[1] + "=")+this._lista_partido_id.ToString(),
								(this.IsNew?"":_columns[2] + "=")+this._categoria_id.ToString(),
                                (this.IsNew?"":_columns[3] + "=")+this._localidad_id.ToString(),
                              };
            return values;
        }

        public string SqlString
        {
            get
            {
                string vvalues = String.Join(",", this.list_values());
                string sqliu = (this.IsNew ? "insert into {0} ({1}) values ({2})" : "update  {0} set {1} where {2}");
                return String.Format(sqliu, this.TableName, (this.IsNew ? String.Join(",", _columns) : vvalues), (this.IsNew ? vvalues : String.Format("id = {0}", this.Id)));
            }
        }

        public void setKeyValue(object valueId)
        {
            throw new NotImplementedException();
        }

        public string sqlKeyWhere(params object[] values)
        {
            return String.Format("id = {0}", values[0]);
        }
    }
}

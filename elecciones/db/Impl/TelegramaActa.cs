﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class TelegramaActa : CommonObj, IAccessDB<TelegramaActa>, ITable
    {
        //"id","nro_mesa", "fecha", "cargado"
        private string[] _columns = { "id", "nro_mesa", "fecha", "cargado" };

        public List<TelegramaActa> findAll()
        {
            return this.findAll(null);
        }
        public List<TelegramaActa> findAll(string criterio)
        {
            return ManagerDB<TelegramaActa>.findAll(criterio);
        }
        public TelegramaActa findbykey(params object[] key)
        {
            //"id","nro_mesa", "fecha", "cargado"
            TelegramaActa ta = (TelegramaActa)ManagerDB<TelegramaActa>.findbyKey(key);
            this.Id = ta.Id;
            this.Nro_Mesa = ta.Nro_Mesa;
            this.Fecha = ta.Fecha;
            this.Cargado = ta.Cargado;
            this.IsNew = false;
            return this;
        }

        public bool saveObj()
        {
            return ManagerDB<TelegramaActa>.saveObject(this);
        }

        public string TableName
        {
            get { return "telegrama_acta"; }
        }

        public string KeyTable
        {
            get { return "id"; }
        }

        public void initialize(System.Data.DataRow dr)
        {
            //"id","nro_mesa", "fecha", "cargado"
            this._id = Int32.Parse(dr[_columns[0]].ToString());
            this._nro_mesa = Int32.Parse(dr[_columns[1]].ToString());
            this._fecha = DateTime.Parse(dr[_columns[2]].ToString());
            this._cargado = Int32.Parse(dr[_columns[3]].ToString()) == 1;
            this.IsNew = false;
        }

        public string[] columns
        {
            get { return _columns; }
        }

        private string[] list_values()
        {
            //"id","nro_mesa", "fecha", "cargado"
            string[] values = { (this.IsNew?"":_columns[0] + "=")+this._id.ToString(),
								(this.IsNew?"":_columns[1] + "=")+this._nro_mesa.ToString(),
                                (this.IsNew?"":_columns[2] + "=")+String.Format("'{0}'",this._fecha.ToString("yyyy-MM-dd")), 
                                (this.IsNew?"":_columns[3] + "=")+(this._cargado ? '1':'0'),

							  };
            return values;
        }

        public string SqlString
        {
            get
            {
                string vvalues = String.Join(",", this.list_values());
                string sqliu = (this.IsNew ? "insert into {0} ({1}) values ({2})" : "update  {0} set {1} where {2}");
                return String.Format(sqliu, this.TableName, (this.IsNew ? String.Join(",", _columns) : vvalues), (this.IsNew ? vvalues : String.Format("id = {0}", this.Id)));
            }
        }

        public void setKeyValue(object valueId)
        {
            throw new NotImplementedException();
        }

        public string sqlKeyWhere(params object[] values)
        {
            return String.Format("id = {0}", values[0]);
        }

    }
}

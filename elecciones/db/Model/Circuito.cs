﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class Circuito 
    {
        //"id","letra", "secc_id", "nombre", "localidad_id"  
        #region variables locales
        private int _id;
		private string _letra;
        private int _secc_id;
        private string _nombre;
        private int _localidad_id;
        #endregion
        
        #region propiedades publicas
				
		public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        
		public string Letra
        {
            get { return _letra; }
            set { _letra = value; }
        }


        public int Secc_Id
        {
            get { return _secc_id; }
            set { _secc_id = value; }
        }

        public string Nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }

        public int Localidad_Id
        {
            get { return _localidad_id; }
            set { _localidad_id = value; }
        }
        #endregion
    }
}

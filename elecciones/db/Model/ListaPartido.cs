﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class ListaPartido
    {
        //"id","partido_id", "nom_lista"
        #region variables locales
        private int _id;
		private int _partido_id;
        private string _nom_lista;
        #endregion
        
        #region propiedades publicas
				
		public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int Partido_Id
        {
            get { return _partido_id; }
            set { _partido_id = value; }
        }

		public string Nom_Lista
        {
            get { return _nom_lista; }
            set { _nom_lista = value; }
        }
        #endregion
    }
}
